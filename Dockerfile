FROM ubuntu:bionic as builder

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y build-essential autoconf libtool libsnmp-dev

COPY . /src
WORKDIR /src

RUN ./autogen.sh
RUN ./configure
RUN make
RUN make install

FROM ubuntu:bionic as runner

RUN apt-get update && \
    apt-get install -y libsnmp30 && \
    rm -rf /var/lib/apt/lists/*

COPY replays /src/replays
COPY --from=builder /src/replayd /usr/local/bin

WORKDIR /src

EXPOSE 1661/udp

CMD /usr/local/bin/replayd -vv
