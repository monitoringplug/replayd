#!/bin/sh

test_description="Test replayd"

. ./sharness.sh

snmp_get() {
  snmpget -Ov -OQ -Oe -Ot -v2c -c system replayd:1661 $1
}

test_expect_success 'It returns Timeticks' '
  test "$(snmp_get .1.3.6.1.2.1.25.1.1.0)" = "25186903"
'

test_expect_success 'It returns IPs' '
  test "$(snmp_get .1.3.6.1.2.1.15.3.1.7.10.1.159.251)" = "10.1.159.251"
'

test_expect_success 'It returns Strings' '
  snmp_get .1.3.6.1.2.1.1.6.0 | grep "Sitting on the Dock of the Bay"
'

test_expect_success 'It returns Integers w/ description' '
  test "$(snmp_get .1.3.6.1.4.1.31865.9999.42.2.1)" = "1"
'

test_expect_success 'It returns negative Integers w/ description' '
  test "$(snmp_get .1.3.6.1.4.1.31865.9999.42.2.2)" = "-2"
'

test_expect_success 'It returns Integers w/o description' '
  test "$(snmp_get .1.3.6.1.4.1.31865.9999.42.2.4)" = "4"
'

test_expect_success 'It returns negative Integers w/o description' '
  test "$(snmp_get .1.3.6.1.4.1.31865.9999.42.2.5)" = "-5"
'

test_expect_success 'It returns Counters' '
  test "$(snmp_get .1.3.6.1.4.1.31865.9999.42.65.1)" = "1"
'

test_expect_success 'It does not return negative Counters' '
  test "$(snmp_get .1.3.6.1.4.1.31865.9999.42.65.2)" = "4294967294"
'

test_expect_success 'It returns Gauge32' '
  test "$(snmp_get .1.3.6.1.4.1.31865.9999.42.66.1)" = "1"
'

test_expect_success 'It does not return negative Gauge32' '
  test "$(snmp_get .1.3.6.1.4.1.31865.9999.42.66.2)" = "4294967294"
'


test_done
